package game;

import csse2002.block.world.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Contains all methods required to generate a map based on
 * a worldmap, and basic components to display it to a window
 * and control it.
 *
 * @author Rinze Watanabe
 */
public class MainApplication extends javafx.application.Application {

    //stores currentmap and x and y position in these variables
    private static WorldMap currentMap;
    private static Integer currentPositionX;
    private static Integer currentPositionY;

    //sets the height and width of each tile
    private final int TILE_WIDTH = 50;
    private final int TILE_HEIGHT = 50;

    //buttons for the arrow
    private Button northButton = new Button("north");
    private Button southButton = new Button("south");
    private Button westButton = new Button("west");
    private Button eastButton = new Button("east");

    //buttons for dig and drop actions
    private Button digButton = new Button("dig");
    private Button dropButton = new Button("drop");

    //menu with menu items for save and load
    private Menu fileMenu = new Menu("File");
    private MenuItem loadGameWorld = new MenuItem("Load Game World");
    private MenuItem saveWorldMap = new MenuItem("Save World Map");
    private int menuCount = 0;

    //set alerts for different errors
    private Alert alert = new Alert(Alert.AlertType.ERROR);
    private Alert alertInformation = new Alert(Alert.AlertType.INFORMATION);

    //label for textfield
    private javafx.scene.control.TextField textField = new javafx.scene.control.TextField("Drop index");

    //combobox to select which action
    private ComboBox actionSelection = new ComboBox();

    //filechooser for choosing map
    private FileChooser fileChooser = new FileChooser();

    //used to get the output from the GUI
    private Stage st;

    /**
     * Shows a warning window.
     *
     * @param warning The String to show to the user.
     */
    private void showWarning(String warning) {
        //set header text of window
        alertInformation.setHeaderText(warning);

        //show warning window
        alertInformation.showAndWait();
    }

    /**
     * Shows a information window.
     *
     * @param information The String to show to the user.
     */
    private void showInformation(String information) {
        //set header text of window
        alertInformation.setHeaderText(information);

        //show information window
        alertInformation.showAndWait();
    }

    /**
     * Show alert window to display exceptions and additional informaiton.
     *
     * @param e                     Exception to be passed and displayed
     * @param additionalInformation Additional informaiton to show to the user.
     */
    private void showAlert(Exception e, String additionalInformation) {
        //set title for alert window
        alert.setTitle("Error");
        alert.setHeaderText("Error: " + e.toString());
        alert.setContentText(additionalInformation);
        alert.showAndWait();
    }

    /**
     * Creates a vbox for saving and loading
     *
     * @return A vbox containing load and save options
     */
    private VBox makeMenu() {
        //only add items once
        if (menuCount == 0) {
            menuCount++;
            fileMenu.getItems().add(loadGameWorld);
            fileMenu.getItems().add(saveWorldMap);
        }

        //add menubar to vbox and return it
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().add(fileMenu);
        VBox vBox = new VBox(menuBar);

        return vBox;

    }

    /**
     * Creates text which shows builder's inventory.
     *
     * @return Text which contains builder's inventory.
     */
    private Text makeInventory() {
        //create text for inventory
        Text inventoryText = new Text();

        //create stringbuilder to append text
        StringBuilder text = new StringBuilder();

        //append first string
        text.append("Builder inventory: ");

        //append each block according to the type of block
        for (Block block : currentMap.getBuilder().getInventory()) {
            if (block instanceof WoodBlock) {
                text.append("Wood, ");
            }
            if (block instanceof SoilBlock) {
                text.append("Soil, ");
            }
            if (block instanceof StoneBlock) {
                text.append("Stone, ");
            }
            if (block instanceof GrassBlock) {
                text.append("Grass, ");
            }
        }

        //convert stringbuilder back to string
        inventoryText.setText(text.toString());

        return inventoryText;
    }

    /**
     * Adjusts position of buttons for the controls of the actions
     *
     * @return A gridpane containing all the required buttons
     */
    private GridPane makeButtons() {
        //set size for direction buttons
        northButton.setMinSize(60, 40);
        southButton.setMinSize(60, 40);
        westButton.setMinSize(60, 40);
        eastButton.setMinSize(60, 40);

        //gridpane to arrange the buttons
        GridPane grid = new GridPane();
        grid.add(northButton, 1, 0);
        grid.add(southButton, 1, 2);
        grid.add(eastButton, 2, 1);
        grid.add(westButton, 0, 1);

        //add dig and drop to gridpane
        grid.add(digButton, 1, 15);
        grid.add(dropButton, 1, 18);

        //add text field to input drop
        grid.add(textField, 3, 18);

        //create items in combobox
        actionSelection.getItems().clear();
        actionSelection.getItems().add("Move builder");
        actionSelection.getItems().add("Move block");

        //set the first selection in the combobox to be the default
        actionSelection.getSelectionModel().selectFirst();

        //add combobox
        grid.add(actionSelection, 3, 14);

        return grid;
    }

    private void refreshWorldMap(WorldMap worldMap, Boolean isInitial) {
        //if the
        if (isInitial) {
            currentPositionX = worldMap.getStartPosition().getX();
            currentPositionY = worldMap.getStartPosition().getY();
        }
        currentMap = worldMap;
    }

    /**
     * Returns tile which at the certain direction corresponding to current direction
     *
     * @param direction Direction in string format.
     * @return Tile in the specified direction.
     */
    private Tile getTileAtDirection(String direction) {
        Position centerPosition = new Position(currentPositionX, currentPositionY);

        //use scangrid to generate map with position
        Map<String, Tile> positionMap = scanGrid(centerPosition, currentMap);

        //iterate through positionMap to look for tile in the direction
        for (Map.Entry<String, Tile> entry : positionMap.entrySet()) {

            //get position from string
            String[] parts = entry.getKey().split(",");
            int xOfTile = Integer.parseInt(parts[0]);
            int yOfTile = Integer.parseInt(parts[1]);

            if (direction.equals("north")) {
                if (currentPositionY - 1 == yOfTile && currentPositionX == xOfTile) {
                    return entry.getValue();
                }
            }

            if (direction.equals("south")) {
                if (currentPositionY + 1 == yOfTile && currentPositionX == xOfTile) {
                    return entry.getValue();
                }
            }

            if (direction.equals("east")) {
                if (currentPositionY == yOfTile && currentPositionX + 1 == xOfTile) {
                    return entry.getValue();
                }
            }

            if (direction.equals("west")) {
                if (currentPositionY == yOfTile && currentPositionX - 1 == xOfTile) {
                    return entry.getValue();
                }
            }
        }
        return null;
    }

    /**
     * Shows the main window.
     *
     * @param stage The stage to add the borderpane to.
     */
    private void showStage(Stage stage) {
        //create position based on current X and Y
        Position currentPosition = new Position(currentPositionX, currentPositionY);

        //creates controls for the main window
        BorderPane borderPane = new BorderPane();
        borderPane.setTop(makeMenu());
        borderPane.setRight(makeButtons());
        VBox vBoxForInventoryText = new VBox(makeInventory());
        borderPane.setBottom(vBoxForInventoryText);

        //creates the map
        GridPane tileGrid = null;
        tileGrid = generateGrid(currentPosition, currentMap);

        //sets the map to the left side of the borderpane
        borderPane.setLeft(tileGrid);

        //add borderpane to the scene and add the the stage
        Scene scene = new Scene(borderPane);
        stage.setScene(scene);

        //show stage
        stage.show();
    }

    /**
     * Scans the tiles and its position surrounding the position.
     *
     * @param centerPosition The position where scangrid is started from.
     * @param worldMap       The target worldmap.
     * @return A map with position and its tile. The position is in a String.
     */
    private Map<String, Tile> scanGrid(Position centerPosition, WorldMap worldMap) {
        SparseTileArray sparseTileArray = new SparseTileArray();

        //create a list of tiles based of center of the grid
        int centerX = centerPosition.getX();
        int centerY = centerPosition.getY();

        Tile centerTile = sparseTileArray.getTile(centerPosition);

        try {
            sparseTileArray.addLinkedTiles(worldMap.getBuilder().getCurrentTile(), centerX, centerY);
        } catch (WorldMapInconsistentException e) {
            e.printStackTrace();
            showAlert(e, "there is an inconsistency in the world");
        }

        //store position of map and tile to the map
        Map<String, Tile> positionMap = new HashMap();
        positionMap.clear();

        //use fromX and fromY to scan the grid from -4<=X<=4 , -4<=Y<=4 so it scans 9*9
        int fromX = -4;
        int fromY = -4;

        while (fromX <= 5) {
            //create a position by using center position and offset it by fromX and fromY
            Position particularPosition = new Position(centerX + fromX, centerY + fromY);

            //if the particular location has a tile.
            if (sparseTileArray.getTile(particularPosition) != null) {
                //create a string "X,Y" and add it to map
                //create string through stringbuilder which contains X and Y, separated with ","
                StringBuilder sb = new StringBuilder();
                sb.append(centerX + fromX);
                sb.append(",");
                sb.append(centerY + fromY);

                //put the position and the tile to the map
                positionMap.put(sb.toString(), sparseTileArray.getTile(particularPosition));
            }

            //when from Y goes to 4, it goes back to -4, and X is incremented. when X goes above 4, the while loop will stop.
            fromY++;
            if (fromY == 5) {
                fromY = -4;
                fromX++;
            }
        }
        return positionMap;

    }

    /**
     * Generates a canvas for the tile according to the information in the tile.
     *
     * @param tile     Tile to generate the canvas for.
     * @param isCenter To determine if the tile is in the center.
     * @return A canvas with all the features(exits, player, block etc.)
     */
    private Canvas generateTileCanvas(Tile tile, Boolean isCenter) {

        //create new canvas and its graphiccontext
        Canvas tileCanvas = new Canvas(TILE_WIDTH, TILE_HEIGHT);

        //rectangle for the outline
        GraphicsContext rectangle = tileCanvas.getGraphicsContext2D();

        //text to indicate number of blocks
        GraphicsContext number = tileCanvas.getGraphicsContext2D();

        // Exits
        GraphicsContext northExit = tileCanvas.getGraphicsContext2D();
        GraphicsContext eastExit = tileCanvas.getGraphicsContext2D();
        GraphicsContext westExit = tileCanvas.getGraphicsContext2D();
        GraphicsContext southExit = tileCanvas.getGraphicsContext2D();

        //circle to indicate builder location
        GraphicsContext builder = tileCanvas.getGraphicsContext2D();

        //rectangle to indicate top block
        GraphicsContext colorRectangle = tileCanvas.getGraphicsContext2D();

        //create rectangle shape
        rectangle.strokeRect(0, 0, TILE_HEIGHT, TILE_WIDTH);

        //set color according to the top block
        Color color = null;
        try {
            if (tile.getTopBlock() instanceof WoodBlock) {
                color = Color.BROWN;
            }
            if (tile.getTopBlock() instanceof GrassBlock) {
                color = Color.GREEN;
            }
            if (tile.getTopBlock() instanceof SoilBlock) {
                color = Color.BLACK;
            }
            if (tile.getTopBlock() instanceof StoneBlock) {
                color = Color.GREY;
            }
        } catch (TooLowException e) {
            e.printStackTrace();
        }

        //stroke number if tiles
        double TILE_WIDTH_DOUBLE = TILE_WIDTH;
        double TILE_HEIGHT_DOUBLE = TILE_HEIGHT;
        number.strokeText(String.valueOf(tile.getBlocks().size()), TILE_WIDTH_DOUBLE / 2, TILE_HEIGHT_DOUBLE / 2);
        number.setStroke(color);

        //strokes exits by letters and sets position in the tile
        for (String exit : tile.getExits().keySet()) {
            if (exit.equals("north")) {
                northExit.setStroke(color);
                northExit.strokeText("N", TILE_WIDTH_DOUBLE * 0.45, TILE_HEIGHT_DOUBLE / 4);
            }
            if (exit.equals("east")) {
                eastExit.setStroke(color);
                eastExit.strokeText("E", TILE_WIDTH_DOUBLE * 0.7, TILE_HEIGHT_DOUBLE / 2);
            }
            if (exit.equals("west")) {
                westExit.setStroke(color);
                westExit.strokeText("W", TILE_WIDTH_DOUBLE * 0.2, TILE_HEIGHT_DOUBLE / 2);
            }
            if (exit.equals("south")) {
                southExit.setStroke(color);
                southExit.strokeText("S", TILE_WIDTH_DOUBLE * 0.45, TILE_HEIGHT_DOUBLE);
            }
        }

        //if the tile is at the center
        if (isCenter) {
            //generates player
            builder.setFill(Color.PINK);
            builder.fillOval(TILE_WIDTH_DOUBLE / 2, TILE_HEIGHT_DOUBLE / 2, TILE_WIDTH_DOUBLE / 3, TILE_HEIGHT_DOUBLE / 3);
        }

        //generates a rectangle to show what the top block is
        colorRectangle.setStroke(color);
        colorRectangle.strokeRect(TILE_WIDTH_DOUBLE / 2, TILE_HEIGHT_DOUBLE / 2, TILE_WIDTH_DOUBLE / 3, TILE_HEIGHT_DOUBLE / 3);

        return tileCanvas;
    }

    /**
     * Creates a grid of canvases based on the position of the builder.
     * The position of the builder is always in the center.
     *
     * @param builderPosition The position of the builder.
     * @param worldMap        The worldmap
     * @return GridPane with all the existing tiles included in position.
     */
    private GridPane generateGrid(Position builderPosition, WorldMap worldMap) {
        //create gridpane to arrange the canvas
        GridPane tileGrid = new GridPane();

        //create map of tiles and its position from scanGrid
        Map<String, Tile> positionMap;
        positionMap = scanGrid(builderPosition, worldMap);

        //add positions of existing tiles into tilerecord
        Map<Integer, Integer> tileRecord = new HashMap<>();
        tileRecord.clear();

        //for each of the entries in positionMap, add the tile and record which positions have been generated
        for (Map.Entry<String, Tile> entry : positionMap.entrySet()) {
            String[] parts = entry.getKey().split(",");

            //convert original position value to gridpane value
            int convertedX = Integer.parseInt(parts[0]) - builderPosition.getX() + 4;
            int convertedY = Integer.parseInt(parts[1]) - builderPosition.getY() + 4;

            //if the tile which is about to be generated is the centerblock
            if (Integer.parseInt(parts[0]) == currentPositionX && Integer.parseInt(parts[1]) == currentPositionY) {
                tileGrid.add(generateTileCanvas(entry.getValue(), true), convertedX, convertedY);
            } else {
                //generate tile using generateTileCanvas
                tileGrid.add(generateTileCanvas(entry.getValue(), false), convertedX, convertedY);
            }

            //record which tiles have been generated
            tileRecord.put(convertedX, convertedY);
        }

        int gridpaneX = 0;
        int gridpaneY = 0;

        //check for empty spaces in the grid
        while (gridpaneX <= 9) {
            for (Map.Entry<Integer, Integer> entry : tileRecord.entrySet()) {

                //if there is a tile already included
                if (entry.getKey() == gridpaneX && entry.getValue() == gridpaneY) {
                    gridpaneY++;
                }
            }

            //empty canvas to create empty space
            Region emptyRegion = new Region();
            emptyRegion.setMinSize(TILE_WIDTH, TILE_HEIGHT);

            //add empty space to tilegrid
            tileGrid.add(emptyRegion, gridpaneX, gridpaneY);

            //when from Y goes to 8, it goes back to -0, and X is incremented. when X goes above 8, the while loop will stop.
            gridpaneY++;
            if (gridpaneY == 10) {
                gridpaneY = 0;
                gridpaneX++;
            }
        }

        return tileGrid;
    }

    /**
     * Starts the main stage and receives actions from buttons.
     *
     * @param stage The main stage
     */
    @Override
    public void start(Stage stage) {

        //set the tile of the window
        stage.setTitle("Game");

        //load map when loadgameworld menu is pressed
        loadGameWorld.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                //opens file chosoer
                File file = fileChooser.showOpenDialog(st);

                if (file != null) {
                    try {
                        WorldMap worldMap = new WorldMap(file.getAbsolutePath());

                        //set currentmap and position
                        refreshWorldMap(worldMap, true);
                        if (currentMap != null) {
                            showInformation("Map loaded!");
                        }

                    } catch (WorldMapFormatException e1) {
                        showAlert(e1, "Map format is invalid");
                        e1.printStackTrace();
                    } catch (WorldMapInconsistentException e1) {
                        showAlert(e1, "Worldmap file is geometrically inconsistant");
                        e1.printStackTrace();
                    } catch (FileNotFoundException e1) {
                        showAlert(e1, "File is not found");
                        e1.printStackTrace();
                    }
                }

                VBox vBoxForInventoryText = new VBox(makeInventory());

                //refresh the GUI
                BorderPane borderPane = new BorderPane();
                borderPane.setTop(makeMenu());
                borderPane.setRight(makeButtons());
                borderPane.setBottom(vBoxForInventoryText);
                borderPane.setLeft(generateGrid(currentMap.getStartPosition(), currentMap));

                Scene scene = new Scene(borderPane);
                stage.setScene(scene);

                stage.show();
            }
        });

        //save map when saveworldmap menu is pressed
        saveWorldMap.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                //if the map is already loaded
                if (currentMap != null) {
                    File file = fileChooser.showOpenDialog(st);
                    if (file != null) {
                        try {
                            currentMap.saveMap(file.getAbsolutePath());
                            showInformation("Map successfully saved");
                        } catch (IOException e) {
                            showAlert(e, "IO exception");
                            e.printStackTrace();
                        }
                    }

                } else {
                    showWarning("Please load a map first");
                }

            }
        });

        //when north button is pressed
        northButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    //if the map is not loaded
                    if (currentMap == null) {
                        showWarning("world is not loaded, please load the map first");
                    }

                    //if movebuilder is selected
                    if (actionSelection.getSelectionModel().getSelectedItem().equals("Move builder")) {

                        //move builder and refresh current position
                        currentMap.getBuilder().moveTo(getTileAtDirection("north"));
                        currentPositionY--;
                    }

                    //if moveblock is selected
                    if (actionSelection.getSelectionModel().getSelectedItem().equals("Move block")) {

                        //move block to north
                        currentMap.getBuilder().getCurrentTile().moveBlock("north");
                    }

                    //show stage
                    showStage(stage);

                    //manage exceptions and show alert windows
                } catch (NoExitException e) {
                    showAlert(e, "No exit on north");
                } catch (InvalidBlockException e) {
                    showAlert(e, "Invalid block");
                } catch (TooHighException e) {
                    showAlert(e, "block too high");
                }
            }
        });

        //when southbutton is pressed
        southButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {

                    //if the map is not loaded
                    if (currentMap == null) {
                        showWarning("world is not loaded, please load the map first");
                    }

                    //if movebuilder is selected
                    if (actionSelection.getSelectionModel().getSelectedItem().equals("Move builder")) {

                        //move builder and refresh current position
                        currentMap.getBuilder().moveTo(getTileAtDirection("south"));
                        currentPositionY++;
                    }

                    //if moveblock is selected
                    if (actionSelection.getSelectionModel().getSelectedItem().equals("Move block")) {

                        //move block to south
                        currentMap.getBuilder().getCurrentTile().moveBlock("south");
                    }

                    //show stage
                    showStage(stage);

                    //handle exceptions by showing alerts
                } catch (NoExitException e) {
                    showAlert(e, "No exit on south");
                } catch (InvalidBlockException e) {
                    showAlert(e, "Invalid block");
                } catch (TooHighException e) {
                    showAlert(e, "block too high");
                }
            }
        });

        //when westbutton is pressed
        westButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {

                    //if the map is not loaded
                    if (currentMap == null) {
                        showWarning("world is not loaded, please load the map first");
                    }

                    //if movebuilder is selected
                    if (actionSelection.getSelectionModel().getSelectedItem().equals("Move builder")) {

                        //move builder and refresh current position
                        currentMap.getBuilder().moveTo(getTileAtDirection("west"));
                        currentPositionX--;
                    }

                    //if moveblock is selected
                    if (actionSelection.getSelectionModel().getSelectedItem().equals("Move block")) {

                        //move block to north
                        currentMap.getBuilder().getCurrentTile().moveBlock("west");
                    }

                    //show stage
                    showStage(stage);

                } catch (NoExitException e) {
                    showAlert(e, "No exit on west");
                } catch (InvalidBlockException e) {
                    showAlert(e, "Invalid block");
                } catch (TooHighException e) {
                    showAlert(e, "block too high");
                }
            }
        });

        //when east is pressed
        eastButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {

                    //if the map is not loaded
                    if (currentMap == null) {
                        showWarning("world is not loaded, please load the map first");
                    }

                    //if movebuilder is selected
                    if (actionSelection.getSelectionModel().getSelectedItem().equals("Move builder")) {

                        //move builder and refresh current position
                        currentMap.getBuilder().moveTo(getTileAtDirection("east"));
                        currentPositionY++;
                    }

                    //if moveblock is selected
                    if (actionSelection.getSelectionModel().getSelectedItem().equals("Move block")) {

                        //move block to east
                        currentMap.getBuilder().getCurrentTile().moveBlock("east");
                    }

                    //show stage
                    showStage(stage);

                    //handle exceptions by showing alerts
                } catch (NoExitException e) {
                    showAlert(e, "No exit on east");
                } catch (InvalidBlockException e) {
                    showAlert(e, "Invalid block");
                } catch (TooHighException e) {
                    showAlert(e, "block too high");
                }
            }
        });

        //when dig is pressed
        digButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {

                    //if the map is not loaded
                    if (currentMap == null) {
                        showWarning("world is not loaded, please load the map first");
                    }

                    //dig from current tile
                    currentMap.getBuilder().digOnCurrentTile();

                    //show alerts
                    showStage(stage);

                    //handle exceptions by showing alerts
                } catch (InvalidBlockException e) {
                    showAlert(e, "Invalid block");
                } catch (TooLowException e) {
                    showAlert(e, "Too low, cannot remove block");
                }
            }
        });

        //when drop is pressed
        dropButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {

                    //if the current map is not loaded
                    if (currentMap == null) {
                        showWarning("world is not loaded, please load the map first");
                    }

                    //drop from inventory, index is in textfield
                    //check if textfield is not empty
                    if (textField.getText() != null) {

                        //checks if textfield only contains numbers
                        if (textField.getText().matches("[0-9]+")) {
                            currentMap.getBuilder().dropFromInventory(Integer.parseInt(textField.getText()));
                        } else {
                            showWarning("textfield is invalid");
                        }
                    } else {
                        showWarning("please enter an integer to drop");
                    }

                    showStage(stage);

                    //handle exceptions by alertwindows
                } catch (InvalidBlockException e) {
                    showAlert(e, "Invalid block");
                } catch (TooHighException e) {
                    showAlert(e, "Too high, cannnot drop anymore");
                }
            }

        });

        //borderpane for the main window
        BorderPane borderPane = new BorderPane();
        borderPane.setTop(makeMenu());
        borderPane.setRight(makeButtons());

        //set borderpane to scene
        Scene scene = new Scene(borderPane);
        stage.setScene(scene);

        stage.show();
    }
}

